package com.Gitlab.Test;

import java.io.IOException;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.Gitlab.Base.Base;
import com.Gitlab.Pages.SignupPage;
//@Listeners(com.Univadis.Listners.MyCustomListners.class)
public class LoginTest extends Base{

	private SignupPage signuppage;
	//private LoginPage loginpage;
	
	@BeforeMethod
	public void setup() throws IOException {
		config();
		signuppage = new SignupPage(driver,wait,wrap);
	}

	//Verify if user click on Register button with entering any input
	@Test(testName="VerifyRegisterWithoutAnyInput",description = "Verify error message without any input provided")
	public void verifyRegisterWithoutInput() throws InterruptedException {
		Assert.assertTrue(signuppage.registerWithoutInput());
	}
	
	//Verify if First name and last name can contains special characters 
	@Test(testName="VerifyFirstname&Lastname",description = "Verify First name and Last name can contains special chracters")
	public void verifyFirtnameLastname() {
		signuppage.verifyFirstnameLastnameText();
	}
	
	//Verify if user enters valid username
	@Test(testName="VerifyValidUsername",description = "Verify no special character in username")
	public void verifyValidUsername() {
		Assert.assertTrue(signuppage.verifyValidUsername());
	}
	
	//Verify if emailidTextbox contains valid email
	@Test(testName="VerifyValidEmail",description = "Verify valid emailid")
	public void verifyValidEmail() {
		Assert.assertTrue(signuppage.verifyValidEmail());
	}
	
	//Varify if password length should be more than 8
	@Test(testName="VerifyPasswordLength",description = "Verify Password Length")
	public void verifyPasswordLengh() {
		Assert.assertTrue(signuppage.verifyValidPasswordLength());
	}

	//Login using Gmail
		@Test(testName="VerifyPasswordLength",description = "Verify Password Length")
		public void loginUsingGmail() {
			signuppage.loginUsingGmail();
			
		}
	
	
	@AfterMethod
	public void quit() {
		tearDown();
	};
	
	
}

