package com.Gitlab.Listners;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.Gitlab.Util.Util;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class MyCustomListners implements ITestListener{
	
	public static ExtentReports reports;
	public static ExtentTest test;
	private final String screenShotPath = System.getProperty("user.dir") + "\\Screenshots\\";
	
	public void onTestStart(ITestResult result) {
		System.out.println("****Test Start****");
		test = reports.createTest(result.getMethod().getMethodName(), result.getMethod().getDescription());
		Util.takeScreenShot(screenShotPath + result.getName() + ".png");
	}

	public void onTestSuccess(ITestResult result) {
		System.out.println("****Test Start****");
		test.log(Status.PASS,result.getName());
		Util.takeScreenShot(screenShotPath + result.getName() + ".png");
	}

	public void onTestFailure(ITestResult result) {
		System.out.println("****Test Fail****");
		test.log(Status.FAIL,result.getName());
		Util.takeScreenShot(screenShotPath + result.getName() + ".png");
	}

	public void onTestSkipped(ITestResult result) {
		System.out.println("****Test Skipped****");
		test.log(Status.SKIP,result.getName());
		Util.takeScreenShot(screenShotPath + result.getName() + ".png");
	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		System.out.println("****Test Failed****");
	}

	public void onStart(ITestContext context) {
		System.out.println("****Suit Start****");
		Date D = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		String formattedDate = formatter.format(D);
		System.out.println("Formatted_date - " + formattedDate);
		ExtentHtmlReporter html = new ExtentHtmlReporter(System.getProperty("user.dir") + "\\ExtentReports\\Test_Report_" + formattedDate + ".html");
		reports = new ExtentReports();
		reports.attachReporter(html);
	}

	public void onFinish(ITestContext context) {
		System.out.println("****Suit Finish****");
		reports.flush();
	}

}
