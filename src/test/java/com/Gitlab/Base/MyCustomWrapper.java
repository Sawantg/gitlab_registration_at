package com.Gitlab.Base;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MyCustomWrapper {
	private WebDriverWait wait;
	private WebDriver driver;

	public MyCustomWrapper(WebDriver driver,int timeOut,int pullingCount) {
		wait = new WebDriverWait(driver,timeOut,pullingCount);
	}
	
	public MyCustomWrapper(WebDriver driver, WebDriverWait wait) {
		this.driver = driver;
		this.wait = wait;
	}
	public WebElement searchElement(By elementLocator) {
		WebElement element = wait.withMessage("Element not able to found webpage")
		.until(ExpectedConditions.visibilityOfElementLocated(elementLocator));
		return element;
	}
	
	public void clickOnElement(By elementLocator) {
		wait.withMessage("Element not able to found webpage")
		.until(ExpectedConditions.elementToBeClickable(elementLocator))
		.click();
	}
	
	public void fillInputBox(By elementLocator,String fillingText) {
		wait.withMessage("Element not able to found webpage")
		.until(ExpectedConditions.visibilityOfElementLocated(elementLocator))
		.sendKeys(fillingText);
	}

	public String getAttributText(By elementLocator, String attributName) {
		String attributText = wait.withMessage("Element not able to found webpage")
		.until(ExpectedConditions.visibilityOfElementLocated(elementLocator))
		.getAttribute(attributName);
		return attributText;
	}

	public void mouseHoverOnElement(WebElement element) {
		Actions act = new Actions(driver);
		act.moveToElement(element).perform();
	}

}
