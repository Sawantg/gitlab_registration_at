package com.Gitlab.Base;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Base {

	protected static WebDriver driver;
	protected WebDriverWait wait;
	protected MyCustomWrapper wrap;
	
	public void config() throws IOException {
			System.setProperty("webdriver.chrome.driver", "C:\\Users\\Administrator\\Desktop\\Drivers\\chromedriver84.exe");
			driver = new ChromeDriver();
			wait = new WebDriverWait(driver,10,1000);
			wrap = new MyCustomWrapper(driver, wait);
			driver.manage().window().maximize();
			driver.manage().timeouts().pageLoadTimeout(20, TimeUnit.SECONDS);
			driver.get("https://gitlab.com/users/sign_up");
	}
	
	public void tearDown() {
		driver.quit();
	}
}
