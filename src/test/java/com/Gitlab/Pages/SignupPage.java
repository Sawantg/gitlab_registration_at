package com.Gitlab.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.Gitlab.Base.MyCustomWrapper;

public class SignupPage {
	
	private WebDriver driver;
	private WebDriverWait wait;
	private MyCustomWrapper wrap;
	
	public SignupPage(WebDriver driver, WebDriverWait wait, MyCustomWrapper wrap) {
		this.driver = driver;
		this.wait = wait;
		this.wrap = wrap;
	}

	public Boolean registerWithoutInput() throws InterruptedException {
		wrap.fillInputBox(By.id("new_user_first_name"), "abc");
		wrap.clickOnElement(By.name("commit"));
		//WebElement errormsgText = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[contains(text(),\"This field is required.\")]")));
		if (driver.findElements(By.xpath("//*[text()=\"This field is required.\"]")).size()>0) {
			return true;
		}
		return false;
	}
	
	public void verifyFirstnameLastnameText() {
		wrap.fillInputBox(By.id("new_user_first_name"), "dadljafjljl#$$$123");
		wrap.fillInputBox(By.id("new_user_last_name"), "asdfjsd@#");
	}
	
	public boolean verifyValidUsername() {
		wrap.fillInputBox(By.id("new_user_username"), "Giz@$"+Keys.ENTER);
		String username = wrap.searchElement(By.xpath("//p[contains(text(),\"Please create a username with only alphanumeric characters.\")]")).getText();
		if (!(username.isEmpty())) 
			return true;
		return false;
	}
	
	public boolean verifyValidEmail() {
		wrap.fillInputBox(By.id("new_user_email"), "Gitesh"+Keys.ENTER);
		WebElement emailTextbox = wrap.searchElement(By.xpath("//p[contains(text(),\"Please provide a valid email address.\")]"));
		if (!(emailTextbox==null)) 
			return true;
		return false;
	}

	public boolean verifyValidPasswordLength() {
		// TODO Auto-generated method stub
		wrap.fillInputBox(By.id("new_user_password"), "Git123"+Keys.ENTER);
		WebElement passwordTextbox = wrap.searchElement(By.xpath("//p[contains(text(),\"Minimum length is 8 characters.\")]"));
		if (!(passwordTextbox==null)) 
			return true;
		
		return false;
	}
	
	public void loginUsingGmail() {
		// TODO Auto-generated method stub
		wrap.clickOnElement(By.id("oauth-login-google_oauth2"));
		wrap.fillInputBox(By.id("identifierId"), "Giteshsawant3092@gmail.com");
		
	}
	
}
