package com.Gitlab.Util;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.annotations.DataProvider;
import com.Gitlab.Base.Base;

public class Util extends Base{
	
	private static final String fileName = "D:\\Automation_Testing\\Java\\Univadis_AT_Revisions\\data\\testdata.xlsx";
	private static final String sheetName = "Sheet1";
	
	public static void takeScreenShot(String path) {
		TakesScreenshot screenShot = (TakesScreenshot) driver;
		File sourceFile = screenShot.getScreenshotAs(OutputType.FILE);
		File destinationFile = new File(path);
		try {
			FileUtils.copyFile(sourceFile, destinationFile);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@DataProvider(name="readExcel")
	public static String[][] readExcel() throws IOException {
		XSSFWorkbook workbook = new XSSFWorkbook(fileName);
		XSSFSheet sheet = workbook.getSheet(sheetName);
		XSSFRow row = sheet.getRow(0);
		XSSFCell cell;
		int noOfRows = sheet.getLastRowNum();
		int noOfCells = row.getLastCellNum();
		String data[][] = new String[noOfRows][noOfCells];
		for (int r=0;r<noOfRows;r++) {
			row = sheet.getRow(r+1);
			for (int c=0;c<noOfCells;c++) {
				cell = row.getCell(c);
				data[r][c]=cell.toString();
				 
			}
			
		}
		workbook.close();
		return data;
		
	}
	
}
